package com.iteratec.swd.demo

import com.github.javafaker.Faker
import com.iteratec.swd.demo.entity.Account
import com.iteratec.swd.demo.repository.AccountRepository
import mu.KotlinLogging
import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Component
import java.util.*

private val logger = KotlinLogging.logger {}

@Component
class DatabaseInitializer(val repository: AccountRepository): CommandLineRunner {
    val faker= Faker(Locale("de-DE"))
    override fun run(vararg args: String?) {
        val address = faker.address()
        val lastname = address.lastName()
        val firstName = address.firstName()
        val description = faker.yoda().quote()
        val account = Account(
            firstname = firstName,
            lastname = lastname,
            login = (firstName.substring(0,1) + lastname).lowercase(),
            description = description
        )
        logger.debug { "User = $account" }
        repository.save(account)
    }
}
