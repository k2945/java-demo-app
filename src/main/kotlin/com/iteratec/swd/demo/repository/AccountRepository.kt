package com.iteratec.swd.demo.repository

import com.iteratec.swd.demo.entity.Account
import org.springframework.data.repository.CrudRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(collectionResourceRel = "users", path = "users")
interface AccountRepository : CrudRepository<Account, Long> {
    fun findByLogin(login: String): Account?
}
