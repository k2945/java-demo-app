package com.iteratec.swd.demo.entity

import org.apache.commons.lang3.builder.ToStringBuilder
import javax.persistence.*

@Entity
class Account(
    var login: String,
    var firstname: String,
    var lastname: String,
    @Column(length = 1024) var description: String? = null,
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) var id: Long? = null) {
    override fun toString() = ToStringBuilder.reflectionToString(this)
}
