package com.iteratec.swd.demo.service

import com.iteratec.swd.demo.repository.AccountRepository
import mu.KotlinLogging
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

private val logger = KotlinLogging.logger {}

@Service
class SampleService(val repository: AccountRepository) {
    @Scheduled(fixedRate = 60000)
    fun query() {
        val users = repository.findAll()
        logger.debug { "There are currently ${users.count()} users in the database" }
    }
}
