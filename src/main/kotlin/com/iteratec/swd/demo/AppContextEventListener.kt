package com.iteratec.swd.demo

import mu.KotlinLogging
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.context.event.EventListener
import org.springframework.core.env.*
import org.springframework.stereotype.Component
import java.util.*
import java.util.Arrays.stream
import java.util.stream.StreamSupport


private val logger = KotlinLogging.logger {}

@Component
class AppContextEventListener {

    @EventListener
    fun handleContextRefresh(event: ContextRefreshedEvent) {
        val env: Environment = event.applicationContext.environment
        logger.info("====== Environment and configuration ======")
        logger.info("Active profiles: {}", Arrays.toString(env.getActiveProfiles()))
        val sources = (env as AbstractEnvironment).propertySources
        StreamSupport.stream(sources.spliterator(), false)
            .filter { ps: PropertySource<*>? -> ps is EnumerablePropertySource<*> }
            .map { ps: PropertySource<*> -> (ps as EnumerablePropertySource<*>).propertyNames }
            .flatMap(Arrays::stream)
            .distinct()
            .filter { prop ->
                !(prop.contains("credentials") || prop.contains(
                    "password"
                ))
            }
            .forEach { prop ->
                logger.info(
                    "{}: {}",
                    prop,
                    env.getProperty(prop)
                )
            }
        logger.info("===========================================")
    }
}
