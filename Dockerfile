#FROM gcr.io/distroless/java
FROM ibm-semeru-runtimes:open-11-jre

ENV TIME_ZONE              Europe/Berlin
ENV SPRING_MAIN_LAZY_INITIATIALIZATION true

WORKDIR /app

COPY ./dependencies/ ./
COPY ./spring-boot-loader/ ./
COPY ./snapshot-dependencies/ ./
COPY ./application/ ./

ENTRYPOINT ["java"]
CMD ["org.springframework.boot.loader.JarLauncher", \
 "-Xscmx32m", \
 "-Xmx256m", \
 "-Xtune:virtualized"]
